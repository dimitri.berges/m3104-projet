<?php
/** Stats (bonus) */

/**
 * Affiche le nombre de candidature par département avec filtre
 *
 * @param array|null $dept Liste des département à filtrer
 * @param boolean $exclude Accepter uniquement ou refuser ces département (par défaut accepter uniquement)
 * @param boolean $sum Avoir chaque département séparés ou une seule valeur à renvoyer (par défault plusieurs valeurs)
 * @param boolean $scene Vrai si recherche dans les scènes, faux pour les départements
 * @return string Chaîne de caractère au format JSON
 */
function stats($dept = null, $exclude = false, $sum = false, $scene = false) {
    header('Content-Type: application/json');
    global $DB_PREFIX;
    if (is_null($dept)) {
        $dept = array();
        $req = Flight::get("db")->query("SELECT ID FROM ${DB_PREFIX}" . ($scene ? "scenes" : "departements") . " ORDER BY ID ASC")->fetchAll();
        foreach ($req as $value) {
            array_push($dept, $value[0]);
        }
    }
    $select = "SELECT";
    $toSelect = ($sum ? "" : ($scene ? "scene" : "dept") . ", ");
    $base_sql = "COUNT(*) as count FROM `${DB_PREFIX}candidatures` WHERE ";
    $fromWhat = ($scene ? "`scene`" : "`dept`");
    $condition = (count($dept)>1 ? ($exclude ? "NOT " : "") . "IN" : ($exclude ? "!" : "") . "=");
    $what2Test = (count($dept)>1 ? "(" . join(", ", $dept) . ")" : "\"${dept[0]}\"");
    $end_sql = ($sum ? "" : ($scene ? "GROUP BY `scene`" : "GROUP BY `dept`"));
    $req = Flight::get("db")->query("$select $toSelect $base_sql $fromWhat $condition $what2Test $end_sql");
    $toJson = array();
    foreach ($req as $departement) {
        if (isset($departement['dept']))
            $toJson[$departement['dept']] = intval($departement['count']);
        elseif (isset($departement['scene']))
            $toJson[$departement['scene']] = intval($departement['count']);
        else
            $toJson["res"] = intval($departement['count']);
    }
    foreach ($dept as $departement) {
        if (!key_exists($departement, $toJson) && !$sum)
            $toJson[$departement] = 0;
    }
    return json_encode($toJson);
};

$stats_all = function () {
    echo stats(null, false, true);
};

$stats_all_depts = function () {
    echo stats();
};

$stats_dept = function ($dept) {
    if (is_numeric($dept))
        echo stats(array($dept));
    else
        echo json_encode(array("error" => "dept number must be numeric"));
};

$stats_hdf = function () {
    echo stats(array(02, 59, 60, 62, 80), false, true);
};

$stats_nhdf = function () {
    echo stats(array(02, 59, 60, 62, 80), true, true);
};

$stats_scenes = function () {
    echo stats(null, false, false, true);
};

$stats_scene = function ($scene) {
    if (is_numeric($scene))
        echo stats(array($scene), false, false, true);
    else
        echo json_encode(array("error" => "scene number must be numeric"));
};
