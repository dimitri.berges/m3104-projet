<!doctype html>
<html>
    <head>
        {block name="head"}{/block}
        <title>{block name="title"}WIP{/block} - Project Festival</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="{$BASE_PATH}/include/pure-min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
    {block name="body"}
        <h1>This page is empty, contact admin for more information.</h1>
        <p>Made by Angélique C, Dimitri B, Michelle M and Théo LM</p>
    {/block}
    </body>
</html>