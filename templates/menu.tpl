{if ($admin)}
<!-- Si admin -->
<div class="menu">
    <a style="text-decoration:none" href="{$BASE_PATH}/logout"><div class="deconnect">DECONNEXION</div></a>
    <a style="text-decoration:none" href="{$BASE_PATH}/"><div class="users">ACCUEIL</div></a>
    <a style="text-decoration:none" href="{$BASE_PATH}/profil"><div class="users">PROFIL</div></a>
    <a style="text-decoration:none" href="{$BASE_PATH}/candidatures"><div class="users">CANDIDATURES</div></a>
    <a style="text-decoration:none" href="{$BASE_PATH}/users"><div class="users">UTILISATEURS</div></a>
</div>
{else}
<!-- Si user -->
<div class="menu">
    <a style="text-decoration:none" href="{$BASE_PATH}/logout"><div class="deconnect">DECONNEXION</div></a>
    <a style="text-decoration:none" href="{$BASE_PATH}/"><div class="users">ACCUEIL</div></a>
    <a style="text-decoration:none" href="{$BASE_PATH}/candidature"><div class="users">MA CANDIDATURE</div></a>
</div>
{/if}