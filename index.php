<?php
session_start();
/** Log pour des raisons de sécurité si sur le site hébergé par Dimitri */
if (strchr($_SERVER['HTTP_HOST'], "iut.lomis.fr"))
    require_once "/home/bedi6130/iut.lomis.fr/log.php";
/** Fin du log */
!isset($_SESSION['last_page']) ? $_SESSION['last_page'] = "/" : 1==1;
require_once "include/flight/flight/Flight.php";
require_once "include/smarty/libs/Smarty.class.php";
require_once "include/pdo.php";
require "routes.php";
require "const.php";

Flight::set("db", $db);

Flight::register("view", "Smarty", array(), function ($smarty) {
    $smarty->template_dir = "./templates/";
    $smarty->compile_dir = "./templates_c/";
    $smarty->config_dir = "./config/";
    $smarty->cache_dir = "./cache/";
    $smarty->assign("BASE_PATH", dirname($_SERVER['SCRIPT_NAME']));
});

Flight::map('render', function ($template, $data=null) {
    Flight::view()->assign($data);
    Flight::view()->display($template);
});

Flight::start();
