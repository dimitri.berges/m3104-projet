<?php
require_once "const.php";
require 'functions/utils.php';

$home = function() {
    Flight::render("templates/index.tpl");
};

/** Connexion & co */
require 'functions/registering.php';

/** Candidature */
require 'functions/candidature.php';

/** Pages utilisateur */
require 'functions/user.php';

/** Pages admin */
require 'functions/admin.php';

/** Liste des utilisateurs */
require 'functions/listes.php';

/** Stats */
require 'functions/stats.php';
